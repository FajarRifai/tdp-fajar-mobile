package com.example.kalkulator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import java.lang.NumberFormatException

class MainActivity : AppCompatActivity() {
    companion object{
        private const val STATE_RESULT = "state_result"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //initial variable
        val edtInput1 = findViewById<EditText>(R.id.edtInput1)
        val edtInput2 = findViewById<EditText>(R.id.edtInput2)
        val txtHasil = findViewById<TextView>(R.id.txtHasil)
        val btnTambah = findViewById<Button>(R.id.btnTambah)
        val btnKurang = findViewById<Button>(R.id.btnKurang)
        val btnKali = findViewById<Button>(R.id.btnKali)
        val btnBagi = findViewById<Button>(R.id.btnBagi)
        val btnClear = findViewById<Button>(R.id.btnClear)

        if (savedInstanceState != null){
            val result = savedInstanceState.getString(STATE_RESULT) as String
            txtHasil.text = result
        }

        //action tambah
        btnTambah.setOnClickListener {
            val input1 = edtInput1.text.toString().trim()
            val input2 = edtInput2.text.toString().trim()

            //function untuk checking data kosong dan tipe data double
            var isEmptyField = false
            var isInvalidDouble = false

            if (input1.isEmpty()){
                isEmptyField = true
                edtInput1.error = "Field Tidak Boleh Kosong"
            }
            if (input2.isEmpty()){
                isEmptyField = true
                edtInput2.error = "Field Tidak Boleh Kosong"
            }

            val a = toDouble(input1)
            val b = toDouble(input2)

            //kondisi jika yang di isikan bukan angka
            if (a === null){
                isInvalidDouble = true
                edtInput1.error = "Isian berupa angka"
            }
            if (b === null){
                isInvalidDouble = true
                edtInput2.error = "Isian berupa angka"
            }

            if (!isEmptyField && !isInvalidDouble){
                val hasil = a as Double + b as Double
                txtHasil.text = hasil.toString()
            }
        }
        //action kurang
        btnKurang.setOnClickListener {
            val input1 = edtInput1.text.toString().trim()
            val input2 = edtInput2.text.toString().trim()

            //function untuk checking data kosong dan tipe data double
            var isEmptyField = false
            var isInvalidDouble = false

            if (input1.isEmpty()){
                isEmptyField = true
                edtInput1.error = "Field Tidak Boleh Kosong"
            }
            if (input2.isEmpty()){
                isEmptyField = true
                edtInput2.error = "Field Tidak Boleh Kosong"
            }

            val a = toDouble(input1)
            val b = toDouble(input2)

            //kondisi jika yang di isikan bukan angka
            if (a === null){
                isInvalidDouble = true
                edtInput1.error = "Isian berupa angka"
            }
            if (b === null){
                isInvalidDouble = true
                edtInput2.error = "Isian berupa angka"
            }

            ////kondisi algoritma variable a dan variable b
            if (!isEmptyField && !isInvalidDouble){
                val hasil = a as Double - b as Double
                txtHasil.text = hasil.toString()
            }
        }
        //action kali
        btnKali.setOnClickListener {
            val input1 = edtInput1.text.toString().trim()
            val input2 = edtInput2.text.toString().trim()

            var isEmptyField = false
            var isInvalidDouble = false

            if (input1.isEmpty()){
                isEmptyField = true
                edtInput1.error = "Field Tidak Boleh Kosong"
            }
            if (input2.isEmpty()){
                isEmptyField = true
                edtInput2.error = "Field Tidak Boleh Kosong"
            }

            val a = toDouble(input1)
            val b = toDouble(input2)

            //kondisi jika yang di isikan bukan angka
            if (a === null){
                isInvalidDouble = true
                edtInput1.error = "Isian berupa angka"
            }
            if (b === null){
                isInvalidDouble = true
                edtInput2.error = "Isian berupa angka"
            }

            //kondisi algoritma variable a dan variable b
            if (!isEmptyField && !isInvalidDouble){
                val hasil = a as Double * b as Double
                txtHasil.text = hasil.toString()
            }
        }
        //action bagi
        btnBagi.setOnClickListener {
            val input1 = edtInput1.text.toString().trim()
            val input2 = edtInput2.text.toString().trim()

            var isEmptyField = false
            var isInvalidDouble = false

            if (input1.isEmpty()){
                isEmptyField = true
                edtInput1.error = "Field Tidak Boleh Kosong"
            }
            if (input2.isEmpty()){
                isEmptyField = true
                edtInput2.error = "Field Tidak Boleh Kosong"
            }

            val a = toDouble(input1)
            val b = toDouble(input2)

            //kondisi jika yang di isikan bukan angka
            if (a === null){
                isInvalidDouble = true
                edtInput1.error = "Isian berupa angka"
            }
            if (b === null){
                isInvalidDouble = true
                edtInput2.error = "Isian berupa angka"
            }
            //kondisi algoritma variable a dan variable b
            if (!isEmptyField && !isInvalidDouble){
                val hasil = a as Double / b as Double
                txtHasil.text = hasil.toString()
            }
        }
        //clear edit text
        btnClear.setOnClickListener {
            edtInput1.setText("")
            edtInput2.setText("")
            txtHasil.setText("0")
            edtInput1.requestFocus()

        }
    }
    //function toDouble untuk checking nilai double
    private fun toDouble(str: String): Double? {
        return try {
            str.toDouble()
        } catch (e: NumberFormatException) {
            null
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val txtHasil = findViewById<TextView>(R.id.txtHasil)
        outState.putString(STATE_RESULT, txtHasil.text.toString())
    }
}